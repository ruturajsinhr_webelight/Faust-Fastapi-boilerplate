import asyncio
from typing import Optional
import faust

_faust_app =faust.App(id="worker")


def get_faust_app() :
    return _faust_app


def set_faust_app_for_api() -> Optional[faust.App]:
    global _faust_app

    # NOTE: the loop argument is needed to ensure that the faust app instance
    # is running in the same event loop as the FastAPI instance.
    _faust_app = faust.App(
        "worker",
        broker="kafka://localhost:9092",
        autodiscover=True,
        # origin="app.v1.user.UserController",
        origin="app.v1.faust",
        loop=asyncio.get_running_loop(),
        value_serializer='raw',
        reply_create_topic=True,
    )

    return _faust_app

faust_app = get_faust_app()
