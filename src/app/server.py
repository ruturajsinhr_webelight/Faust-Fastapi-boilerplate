from fastapi import Depends, FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from pydantic import ValidationError

from app import router
from app.faust import set_faust_app_for_api, get_faust_app
from config import AppEnvironment, config
from core.exceptions import CustomException
from core.fastapi.dependencies import Logging
from core.helpers.cache import Cache, CustomKeyMaker, RedisBackend

import asyncio

fastapi_app = FastAPI()


def init_cors(app: FastAPI) -> None:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


def init_routers(app: FastAPI) -> None:
    app.include_router(router)


def init_listeners(app: FastAPI) -> None:
    # Exception handler
    @app.exception_handler(CustomException)
    async def custom_exception_handler(request: Request, exc: CustomException):
        return JSONResponse(
            status_code=exc.code,
            content={"error_code": exc.error_code, "message": exc.message},
        )

    @app.exception_handler(ValidationError)
    async def validation_error_handler(request: Request, exc: ValidationError):
        return JSONResponse(
            status_code=400,
            content={"error_code": 400, "message": exc.json()},
        )

@fastapi_app.on_event("startup")
async def startup():
    # set up the faust app
    set_faust_app_for_api()

    faust_app = get_faust_app()

    # start the faust app in client mode
    asyncio.create_task(
        faust_app.start_client()
    )

@fastapi_app.on_event("shutdown")
async def shutdown():
    faust_app = get_faust_app()

    # graceful shutdown
    await faust_app.stop()

def on_auth_error(request: Request, exc: Exception):
    status_code, error_code, message = 401, None, str(exc)
    if isinstance(exc, CustomException):
        status_code = int(exc.code)
        error_code = exc.error_code
        message = exc.message

    return JSONResponse(
        status_code=status_code,
        content={"error_code": error_code, "message": message},
    )


def init_cache() -> None:
    Cache.init(backend=RedisBackend(), key_maker=CustomKeyMaker())


# def create_app(get_faust: get_faust_app(), set_faust: set_faust_app_for_api()) -> FastAPI:
#     app = FastAPI(
#         title="Hide",
#         description="Hide API",
#         version="1.0.0",
#         docs_url=None if config.ENV == AppEnvironment.Production else "/docs",
#         redoc_url=None if config.ENV == AppEnvironment.Production
#         else "/redoc",
#         dependencies=[Depends(Logging)],
#
#     )
#
#     asyncio.create_task(
#         faust_app.start_client()
#     )
#
#     init_routers(app=app)
#     init_cors(app=app)
#     init_listeners(app=app)
#     init_cache()
#     return app
#
#
# app = create_app(get_faust=faust_app, set_faust=set_faust_app_for_api())

def create_app() -> FastAPI:
    app = FastAPI(
        title="Hide",
        description="Hide API",
        version="1.0.0",
        docs_url=None if config.ENV == AppEnvironment.Production else "/docs",
        redoc_url=None if config.ENV == AppEnvironment.Production
        else "/redoc",
        dependencies=[Depends(Logging)],

    )
    init_routers(app=app)
    init_cors(app=app)
    init_listeners(app=app)
    init_cache()
    return app


app = create_app()
