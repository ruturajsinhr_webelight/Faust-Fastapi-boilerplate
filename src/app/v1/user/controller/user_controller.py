from typing import NoReturn, Union

from fastapi import Depends
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter

from app.v1.user.schema import CreateUserRequest, CreateUserResponse, User
# from app.v1.user.service.user_command import UserCommandService
# from app.v1.user.service.user_command import UserCommandService
from core.fastapi.schemas.response import ExceptionResponseSchema
import importlib

router = InferringRouter()


@cbv(router=router)
class UserController:
    @router.post(
        "/",
        # response_model=CreateUserResponse,
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Create User",
    )
    async def create_user(
            self,
            # request: CreateUserRequest,
            # service: UserCommandService = Depends(UserCommandService),
    ):
        get_current_count_task = importlib.import_module(
            "app.v1.user.service.createuser",
        )
        await get_current_count_task.agent()

        return {"User": "Created"}

    @router.post(
        "/greet",
        # response_model=CreateUserResponse,
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Greet",
    )
    async def greet(
            self,
            # request: CreateUserRequest,
            # service: UserCommandService = Depends(UserCommandService),
    ):
        return {"Result": "Greetings"}
