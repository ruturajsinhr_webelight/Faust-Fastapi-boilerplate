from neo4j import GraphDatabase

from app.faust import get_faust_app


def connection():
    driver = GraphDatabase.driver(uri="bolt://localhost:7687", auth=("neo4j", "Rutu@123"))
    return driver


faust_app = get_faust_app()

topics = faust_app.topic("createuser", key_type=bytes, value_type=bytes)

print(topics)

@faust_app.agent(channel=topics)
async def agent(stream):
    print(stream)
    print(type(stream))
    print(i async for i in stream)
    driver_neo4j = connection()
    session = driver_neo4j.session()
    q = """
    merge(n:User{name:$name})
    """
    async for greet in stream:
        print("in async for")
        print(greet)
        print(stream)
        print(greet.decode())
        x = {"name": greet.decode()}
        session.run(q, x)
        break
    print("loop completed")
